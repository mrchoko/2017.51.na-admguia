<?php

require(dirname(__DIR__)."/Controladores/lib/BarcodeGenerator.php");

require_once(dirname(__DIR__)."/Controladores/lib/BarcodeGeneratorJPG.php");

if(file_exists('../config/conexion.php')){
	require_once('../config/conexion.php');
}
if(file_exists('../modelos.php')){
	require_once('../modelos.php');
}


class Controlador_Base{

	public function activa($id=False,$tabla=False,$nombre_base_datos=False){
		$now = time();
		if($now > $_SESSION['expire']) {
			session_destroy();
			header('Location: views/session/');
		}
		else{
			$conexion = new Conexion();
			$conexion->selecciona_base_datos($nombre_base_datos);

			$modelo = new Modelos();
			$resultado = $modelo->activa_db($id,$tabla);

			return $resultado;
		}
	}

	public function desactiva($id,$tabla,$nombre_base_datos){
		$now = time();
		if($now > $_SESSION['expire']) {
			session_destroy();
			header('Location: views/session/');
		}
		else{
			$conexion = new Conexion();
			$conexion->selecciona_base_datos($nombre_base_datos);

			$modelo = new Modelos();
			$resultado = $modelo->desactiva_db($id,$tabla);

			return $resultado;
		}
	}

	public function redirect($registro, $accion, $tabla, $operacion){

		if($registro){
			header('Location: index.php?seccion='.$tabla.'&accion='.$accion.'&resultado=correcto&operacion='.$operacion);
		}
		else{
			header('Location: index.php?seccion='.$tabla.'&accion='.$accion.'&resultado=incorrecto&operacion='.$operacion);	
		}

	}

	public function desactiva_db(){
		$now = time();
		if($now > $_SESSION['expire']) {
			session_destroy();
			header('Location: views/session/');
		}
		else{
			$tabla = $_GET['seccion'];
			$registro_id = $_GET[$tabla.'_id'];

			$registro = $this->desactiva($registro_id, $tabla);		
		}
	}	

	public function elimina($id,$tabla){
		$now = time();
		if($now > $_SESSION['expire']) {
			session_destroy();
			header('Location: views/session/');
		}
		else{
			$conexion = new Conexion();
			$conexion->selecciona_base_datos();

			$modelo = new Modelos();
			$modelo->elimina_db($tabla,$id);
			$registro_obtenido = $modelo->obten_por_id($tabla,$id);
			
			if (!$registro_obtenido) {
				return true;
			}
			else{
				return false;
			}
		}
	}

	public function elimina_bd(){
		$now = time();
		if($now > $_SESSION['expire']) {
			session_destroy();
			header('Location: views/session/');
		}
		else{
			$tabla = $_GET['seccion'];
			$registro_id = $_GET[$tabla.'_id'];

			$registro = $this->elimina($registro_id,$tabla);

			if ($registro) {
				if ($tabla === 'guia' ) {
					$directorio_codigo_barras = dirname(__DIR__).'/views/guia/codigo_barras/';
					$delete_codigo_barras = $_GET['codigo_barras'];
					unlink($directorio_codigo_barras.'/'.$delete_codigo_barras);
				}
			}

			$this->redirect($registro, 'lista', $tabla,'Elimina');
		}
	}	

	public function inserta($registro,$tabla,$nombre_base_datos=false){
		$now = time();
		if($now > $_SESSION['expire']) {
			session_destroy();
			header('Location: views/session/');
		}
		else{
			$conexion = new Conexion();
			$conexion->selecciona_base_datos($nombre_base_datos);
			$modelo = new Modelos();
			$registro_id = $modelo->alta_db($registro,$tabla,$nombre_base_datos);
			return $registro_id;

		}
	}
	public function modifica($registro,$tabla,$nombre_base_datos){
		$now = time();
		if($now > $_SESSION['expire']) {
			session_destroy();
			header('Location: views/session/');
		}
		else{
			$conexion = new Conexion();
			$conexion->selecciona_base_datos($nombre_base_datos);

			$modelo = new Modelos();
			$registro_id = $modelo->modifica_db($registro,$tabla);

			if($registro_id){
				$registro_obtenido = $modelo->obten_por_id($tabla,$registro_id,$nombre_base_datos);
				$registro_enviar[$tabla] = $registro_obtenido;
				return $registro_enviar;
			}
			else{
				return false;
			}
		}
	}

	public function alta_bd(){
		$now = time();
		if($now > $_SESSION['expire']) {
			session_destroy();
			header('Location: views/session/');
		}
		else{
			$campos = $this->obten_estructura($_GET['seccion']);
			$registro = $this->genera_registro_alta($campos);
			$tabla = $this->inserta($registro,$_GET['seccion']);
			$this->redirect($tabla, 'lista', $_GET['seccion'],'Guarda');
		}
	}


	public function genera_registro_alta($campos_grupo){
		foreach ($campos_grupo as $key => $campo) {
			if($key!='id'){
				if($key=='status'){
					$registro[$key] = (isset($_POST['status'])) ? 1 : 0;
				}elseif ($key=='guia') {
					$registro[$key] = mktime().rand(0,9);
					$this->genera_imagen_codigo_barras($registro[$key]);

				}elseif ($key=='estado_id') {
					if (empty($_POST[$key])) {
						$registro[$key] = 1;
					}else{
						$registro[$key] = $_POST[$key];
					}
				}elseif ($key=='facturado') {
					if (empty($_POST[$key])) {
						$registro[$key] = 0;
					}else{
						$registro[$key] = $_POST[$key];
					}
				}elseif ($key=='timbrado') {
					if (empty($_POST[$key])) {
						$registro[$key] = 0;
					}else{
						$registro[$key] = $_POST[$key];
					}
				}
				else{
					$registro[$key] = $_POST[$key];	
				}
			}
		}
		return $registro;
	}

	public function obten_estructura($tabla){
		$modelo = new modelos();
		$db = $modelo->genera_estructura();
		$estructura_grupo = $db[$tabla];
		$campos_grupo = $estructura_grupo['campos'];
		return $campos_grupo;
	}

	public function genera_imagen_codigo_barras($numero_guia){
		$generator = new BarcodeGeneratorJPG();					

		$directorio = dirname(__DIR__).'/views/guia/codigo_barras/';
		$nombre_archivo=$numero_guia.'.jpg';

		$barcode = $generator->getBarcode($numero_guia, $generator::TYPE_CODE_128);
		file_put_contents($directorio.''.$nombre_archivo, $barcode);
		
	}

}

?>