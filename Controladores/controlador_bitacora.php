<?php 
require_once('controlador_base.php');
require_once('controlador_guia.php');
require_once('controlador_estado.php');

if(file_exists('./config/conexion.php')){
	require_once('./config/conexion.php');
}
if(file_exists('./config/conexion.php')){
	require_once('./modelos.php');
}
class Controlador_Bitacora extends Controlador_Base{
	public function lista_bitacora(){

		$conexion = new Conexion();
		$conexion->selecciona_base_datos();
		$modelo = new modelos();
		$registro_obtenido = $modelo->genera_lista_bitacora();
		$registro_enviar = $registro_obtenido;
		return $registro_enviar;
	}
}
$bitacora_controller = new Controlador_Bitacora();

if($accion == 'alta' ){
	$controller_guia = new Controlador_Guia();
	$guias = $controller_guia->lista_guia();
	$controller_estado = new Controlador_Estado();
	$estados = $controller_estado->lista_estado();
}

if($accion == 'lista' && $seccion == 'bitacora'){
	$bitacoras = $bitacora_controller->lista_bitacora();
}

?>
