<?php
require_once('controlador_base.php');

if(file_exists('./config/conexion.php')){
	require_once('./config/conexion.php');
}
if(file_exists('./config/conexion.php')){
	require_once('./modelos.php');
}
class Controlador_Estado extends Controlador_Base{
	public function lista_estado(){
		$conexion = new Conexion();
		$conexion->selecciona_base_datos();
		$modelo = new modelos();
		$registro_obtenido = $modelo->genera_lista_estado();
		$registro_enviar = $registro_obtenido;
		return $registro_enviar;
	}

}


$estado_controller = new Controlador_Estado();


if($accion == 'lista' && $seccion == 'estado'){
	$estados = $estado_controller->lista_estado();
}


if($accion == 'modifica' && $seccion == 'estado'){
	$estado_id = $_GET['estado_id'];

	$conexion = new Conexion();
	$conexion->selecciona_base_datos();

	$modelo = new Modelos();

	$estados = $modelo->obten_por_id('estado',$estado_id);

}

if($accion == 'modifica_bd' ){
	$estado_id = $_GET['estado_id'];
	$descripcion = $_POST['descripcion'];

	$registro = array(
		'id'=>$estado_id,'descripcion'=>$descripcion);
	$tabla = 'estado';
	$estados = $estado_controller->modifica($registro,$tabla,$nombre_base_datos);

	if($estados){
		$estado_controller->redirect($registro, 'lista', 'estado','modifica');
	}
	else{
		$estado_controller->redirect($registro, 'lista', 'estado','modifica');
	}
}

?>

