<?php
require_once('controlador_base.php');
require_once('controlador_seccion.php');
require_once('controlador_accion.php');
if(file_exists('./config/conexion.php')){
	require_once('./config/conexion.php');
}
if(file_exists('./config/conexion.php')){
	require_once('./modelos.php');
}
class Controlador_Grupo extends Controlador_Base{

	public function lista_grupo(){

		$conexion = new Conexion();
		$conexion->selecciona_base_datos();
		$modelo = new modelos();
		$registro_obtenido = $modelo->genera_lista_grupo();
		$registro_enviar = $registro_obtenido;
		return $registro_enviar;
	}

}

$grupo_controller = new Controlador_Grupo();
$controller_seccion = new Controlador_Seccion();
$controller_accion = new Controlador_Accion();
$controller_accion_grupo = new Modelos();


if( ($accion == 'lista' || $accion == 'asigna_accion') && $seccion=='grupo'){
	$grupos = $grupo_controller->lista_grupo();
	$secciones = $controller_seccion->lista_seccion();
	$acciones = $controller_accion->lista_accion();
	$acciones_grupos = $controller_accion_grupo->genera_lista_accion_grupo();
}

if($accion == 'desactiva'){
	$grupo_id = $_GET['grupo_id'];
	$grupo = $grupo_controller->desactiva($grupo_id,'grupo');
	if($grupo){
		$grupo_controller->redirect($registro, 'lista', 'grupo','desactiva');
	}
	else{
		$grupo_controller->redirect($registro, 'lista', 'grupo','desactiva');
	}	
}

if($accion == 'activa'){
	$grupo_id = $_GET['grupo_id'];
	$grupo = $grupo_controller->activa($grupo_id,'grupo');
	if($grupo){
		$grupo_controller->redirect($registro, 'lista', 'grupo','activa');
	}
	else{
		$grupo_controller->redirect($registro, 'lista', 'grupo','activa');
	}		
}

if( ($accion == 'modifica' || $accion == 'asigna_accion' ) && $seccion == 'grupo'){
	$grupo_id = $_GET['grupo_id'];

	$conexion = new Conexion();
	$conexion->selecciona_base_datos();

	$modelo = new Modelos();

	$grupo = $modelo->obten_por_id('grupo',$grupo_id);

}

if($accion == 'modifica_bd'){
	$grupo_id = $_GET['grupo_id'];
	$descripcion = $_POST['descripcion'];
	$observaciones = $_POST['observaciones'];
	$status=$_POST['status'];

	$registro = array(
		'id'=>$grupo_id,'descripcion'=>$descripcion, 'observaciones'=>$observaciones, 'status'=>$status);
	$tabla = 'grupo';
	$grupo = $grupo_controller->modifica($registro,$tabla);
	if($grupo){
		$grupo_controller->redirect($registro, 'lista', 'grupo','modifica');
	}
	else{
		$grupo_controller->redirect($registro, 'lista', 'grupo','modifica');
	}	
}
?>