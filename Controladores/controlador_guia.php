<?php 
require_once('controlador_base.php');
require_once('controlador_cliente.php');
require_once('controlador_forma_pago.php');

if(file_exists('./config/conexion.php')){
	require_once('./config/conexion.php');
}
if(file_exists('./config/conexion.php')){
	require_once('./modelos.php');
}
class Controlador_Guia extends Controlador_Base{
	public function lista_guia(){

		$conexion = new Conexion();
		$conexion->selecciona_base_datos();
		$modelo = new modelos();
		$registro_obtenido = $modelo->genera_lista_guia();
		$registro_enviar = $registro_obtenido;
		return $registro_enviar;
	}

	private function obten_contenido_archivo($ruta_plantilla){

		$plantilla = fopen($ruta_plantilla, "r");
		$contenido_plantilla = fread($plantilla, filesize($ruta_plantilla));
		fclose($plantilla);
		return $contenido_plantilla;


	}

	public function genera_factura(){
		$guia_id = $_GET['guia_id'];
		$contenido_plantilla = $this->obten_contenido_archivo('./plantillas/facturaa.xml');
		$modelo = new Modelos();
		$guias = $modelo->obten_por_id('guia', $guia_id);

		$guia = $guias['0'];

		$forma_pago_id = $guia['forma_pago_id'];
		$metodo_pago = $guia['metodo_pago'];
		$sub_total = $guia['subtotal'];
		$iva = $sub_total * .16;
		$total = $sub_total + $iva;	
		$cliente_id = $guia['cliente_id'];	

		$clientes = $modelo->obten_por_id('cliente',$cliente_id);
	
		$rfc_receptor = $clientes[0]['rfc'];
		$razon_social_receptor = $clientes[0]['razon_social'];

		$folio = $guia_id;
		$fecha = date('Y-m-d H:m:i');
		$fecha = str_replace(' ', 'T', $fecha);
		$formaDePago = $forma_pago_id;
		$metodoPago = $metodo_pago;
		$Total = $total;
		$SubTotal = $sub_total;
		$Rfc_receptor = strtoupper($rfc_receptor);
		$Razon_social_receptor = strtoupper($razon_social_receptor);

		$alto_paquete = $guia['alto_paquete'];
		$ancho_paquete = $guia['ancho_paquete'];
		$largo_paquete = $guia['largo_paquete'];
		$peso_paquete = $guia['peso_paquete'];
		$descripcion_paquete = $guia['descripcion'];
		$calle = $guia['calle'];
		$numero_ext = $guia['numero_ext'];
		$numero_int = $guia['numero_int'];
		$cp = $guia['codigo_postal'];
		$colonia = $guia['colonia'];
		$destinatario = $guia['destinatario'];

		$descripcion_partida = "Envio de paquete con las siguientes caracteristicas: 
		Alto del paquete: $alto_paquete cms, Ancho del paquete: $ancho_paquete cms, 
		Largo del paquete: $largo_paquete cms, peso del paquete: $peso_paquete grs, 
		con $descripcion_paquete, en calle: $calle, numero: $numero_ext, 
		interior: $numero_int, cp: $cp, colonia: $colonia a $destinatario";

		$n_identificacion = $guia['guia'];
		$factura_xml = $contenido_plantilla;
		$factura_xml = str_replace('|Folio|', $folio, $factura_xml);
		$factura_xml = str_replace('|iva|', $iva, $factura_xml);
		$factura_xml = str_replace('|Fecha|', $fecha, $factura_xml);
		$factura_xml = str_replace('|FormaPago|', $formaDePago, $factura_xml);
		$factura_xml = str_replace('|MetodoPago|', $metodoPago, $factura_xml);
		$factura_xml = str_replace('|Total|', $Total, $factura_xml);
		$factura_xml = str_replace('|SubTotal|', $SubTotal, $factura_xml);
		$factura_xml = str_replace('|Rfc_receptor|', $Rfc_receptor, $factura_xml);
		$factura_xml = str_replace('|Razon_social_receptor|', $Razon_social_receptor, $factura_xml);
		$factura_xml = str_replace('|n_identificacion|', $n_identificacion, $factura_xml);
		$factura_xml = str_replace('|descripcion_partida|', $descripcion_partida, $factura_xml);

		

		$xml_sin_timbrar = fopen('./facturas/'.$guia_id.'.xml', 'w');
		fwrite($xml_sin_timbrar, $factura_xml);
		fclose($xml_sin_timbrar);

	}

	public function genera_reporte(){
		$guia_id = $_GET['guia_id'];
		$modelo = new Modelos();
		$guias = $modelo->obten_por_id('guia', $guia_id);

		$guia = $guias['0'];


	}

}


$guia_controller = new Controlador_Guia();


if($accion == 'genera_factura'){
	$guia_id = $_GET['guia_id'];
	$conexion = new Conexion();
	$conexion->selecciona_base_datos();
	$modelo = new modelos();
	$link = $conexion->link;
	$guia = $guia_controller->genera_factura('guia', $guia_id);

	$link -> query("UPDATE guia SET facturado = '1' WHERE id = $guia_id");
	
}

if ($accion == 'genera_reporte') {
	$guia_controller->genera_factura('guia', $guia_id);	
	
}

if($accion == 'alta' ){
	$controller_cliente = new Controlador_Cliente();
	$clientes = $controller_cliente->lista_cliente();
	$controller_forma_pago = new Controlador_Forma_Pago();
	$formas = $controller_forma_pago->lista_forma_pago();
}

if($accion == 'lista' && $seccion == 'guia'){
	$guias = $guia_controller->lista_guia();
}
	
if($accion == 'colonia' && $seccion == 'guia'){
	$conexion = new Conexion();
	$conexion->selecciona_base_datos();
	$modelo = new modelos();
	$link = $conexion->link;
	mysqli_set_charset($link,'utf8');

	$colonias;
	$municipios;
	$estados;

	$consulta_colonias = "SELECT asentamiento FROM codigo WHERE cp = '".$_POST['codigo_postal']."'";
	$consulta_municipios = "SELECT municipio FROM codigo WHERE cp = '".$_POST['codigo_postal']."' group by municipio";
	$consulta_estados = "SELECT estado FROM codigo WHERE cp = '".$_POST['codigo_postal']."' group by estado";


	$result_colonias = $link->query($consulta_colonias);
	$result_municipios = $link->query($consulta_municipios);
	$result_estados = $link->query($consulta_estados);

	$numero_registros_colonias = $result_colonias->num_rows;
	if($numero_registros_colonias>0)
	{
		$i=0;
		$tabla_base = array('asentamiento');
		while($row = $result_colonias->fetch_array()){
			foreach ($tabla_base as $campo => $valor) {
				$colonias[$i]['colonia'] = $row[$campo];
			}
			$i++;
		}
		while($municipio = mysqli_fetch_assoc($result_municipios))
		{
			$name = $municipio["municipio"];	
			$municipios = array('0' => array('municipio' =>  $name  ));
		}

		while($estado = mysqli_fetch_assoc($result_estados))
		{
			$name = $estado["estado"];				
			$estados = array('0' => array('estado' =>  $name  ));
		}
		header('Content-type: application/json; charset=utf-8');
		echo json_encode(array("resultado"=>true,"colonias"=>$colonias,"municipio"=> $municipios,"estado"=> $estados));
		exit();
	}
	else
	{
		header('Content-type: application/json; charset=utf-8');
		echo json_encode(array("resultado"=>false));
		exit();
	}
}

?>