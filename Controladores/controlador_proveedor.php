<?php 
require_once('controlador_base.php');

if(file_exists('./config/conexion.php')){
	require_once('./config/conexion.php');
}
if(file_exists('./config/conexion.php')){
	require_once('./modelos.php');
}
class Controlador_Proveedor extends Controlador_Base{
	public function lista_proveedor(){

		$conexion = new Conexion();
		$conexion->selecciona_base_datos();
		$modelo = new modelos();
		$registro_obtenido = $modelo->genera_lista_proveedor();
		$registro_enviar = $registro_obtenido;
		return $registro_enviar;
	}
}
$proveedor_controller = new Controlador_Proveedor();

if($accion == 'lista' && $seccion == 'proveedor'){
	$proveedores = $proveedor_controller->lista_proveedor();
}

if($accion == 'modifica' && $seccion == 'proveedor'){
	$proveedor_id = $_GET['proveedor_id'];

	$conexion = new Conexion();
	$conexion->selecciona_base_datos();
	$link = $conexion->link;
	mysqli_set_charset($link,'utf8');

	$modelo = new Modelos();

	$proveedores = $modelo->obten_por_id('proveedor',$proveedor_id );

	$consulta_colonias = "SELECT asentamiento FROM codigo WHERE cp = '".$proveedores[0]['codigo_postal']."'";
	$consulta_municipios = "SELECT municipio FROM codigo WHERE cp = '".$proveedores[0]['codigo_postal']."' group by municipio";
	$consulta_estados = "SELECT estado FROM codigo WHERE cp = '".$proveedores[0]['codigo_postal']."' group by estado";

	$result_colonias = $link->query($consulta_colonias);
	$result_municipios = $link->query($consulta_municipios);
	$result_estados = $link->query($consulta_estados);

	$i=0;
	$tabla_base = array('asentamiento');
	while($row = $result_colonias->fetch_array()){
		foreach ($tabla_base as $campo => $valor) {
			$colonias[$i]['colonia'] = $row[$campo];
		}
		$i++;
	}
	while($municipio = mysqli_fetch_assoc($result_municipios))
	{
		$name = $municipio["municipio"];	
		$municipios = array('0' => array('municipio' =>  $name  ));
	}

	while($estado = mysqli_fetch_assoc($result_estados))
	{
		$name = $estado["estado"];				
		$estados = array('0' => array('estado' =>  $name  ));
	}

}

if($accion == 'modifica_bd' && $seccion == 'proveedor'){
	$proveedor_id = $_GET['proveedor_id'];
	$nombre = $_POST['nombre'];
	$apellido_paterno = $_POST['apellido_paterno'];
	$apellido_materno = $_POST['apellido_materno'];
	$telefono = $_POST['telefono'];
	$email = $_POST['email'];
	$calle = $_POST['calle'];
	$numero_ext = $_POST['numero_ext'];
	$numero_int = $_POST['numero_int'];
	$codigo_postal = $_POST['codigo_postal'];
	$colonia = $_POST['colonia'];
	$rfc = $_POST['rfc'];
	$razon_social = $_POST['razon_social'];
	$nombre_contacto = $_POST['nombre_contacto'];
	$correo_contacto = $_POST['correo_contacto'];
	$pagina_web = $_POST['pagina_web'];
	$banco = $_POST['banco'];
	$cuenta_bancaria = $_POST['cuenta_bancaria'];
	$cabe=$_POST['clabe'];

	$registro = array(
		'id'=>$proveedor_id,'nombre'=>$nombre, 'apellido_paterno'=>$apellido_paterno, 'apellido_materno'=>$apellido_materno, 'telefono'=>$telefono,'email'=>$email, 'calle'=>$calle,'numero_ext'=>$numero_ext, 'numero_int'=>$numero_int, 'codigo_postal'=>$codigo_postal, 'colonia'=>$colonia, 'rfc'=>$rfc,'razon_social'=>$razon_social, 'nombre_contacto'=>$nombre_contacto, 'correo_contacto'=>$correo_contacto,'pagina_web'=>$pagina_web, 'banco'=>$banco, 'cuenta_bancaria'=>$cuenta_bancaria, 'clabe'=>$clabe);
	$tabla = 'proveedor';
	$proveedores = $proveedor_controller->modifica($registro,$tabla,$nombre_base_datos);
	if($proveedores){
		$proveedor_controller->redirect($registro, 'lista', 'proveedor','modifica');
	}
	else{
		$proveedor_controller->redirect($registro, 'lista', 'proveedor','modifica');
	}
}
	
if($accion == 'colonia' && $seccion == 'proveedor'){
	$conexion = new Conexion();
	$conexion->selecciona_base_datos();
	$modelo = new modelos();
	$link = $conexion->link;
	mysqli_set_charset($link,'utf8');
	$colonias;
	$municipios;
	$estados;

	$consulta_colonias = "SELECT asentamiento FROM codigo WHERE cp = '".$_POST['codigo_postal']."'";
	$consulta_municipios = "SELECT municipio FROM codigo WHERE cp = '".$_POST['codigo_postal']."' group by municipio";
	$consulta_estados = "SELECT estado FROM codigo WHERE cp = '".$_POST['codigo_postal']."' group by estado";

	$result_colonias = $link->query($consulta_colonias);
	$result_municipios = $link->query($consulta_municipios);
	$result_estados = $link->query($consulta_estados);

	$numero_registros_colonias = $result_colonias->num_rows;
	if($numero_registros_colonias>0)
	{
		$i=0;
		$tabla_base = array('asentamiento');
		while($row = $result_colonias->fetch_array()){
			foreach ($tabla_base as $campo => $valor) {
				$colonias[$i]['colonia'] = $row[$campo];
			}
			$i++;
		}
		while($municipio = mysqli_fetch_assoc($result_municipios))
		{
			$name = $municipio["municipio"];	
			$municipios = array('0' => array('municipio' =>  $name  ));
		}

		while($estado = mysqli_fetch_assoc($result_estados))
		{
			$name = $estado["estado"];				
			$estados = array('0' => array('estado' =>  $name  ));
		}
		header('Content-type: application/json; charset=utf-8');
		echo json_encode(array("resultado"=>true,"colonias"=>$colonias,"municipio"=> $municipios,"estado"=> $estados));
		exit();
	}
	else
	{
		//return false;
		header('Content-type: application/json; charset=utf-8');
		echo json_encode(array("resultado"=>false));
		exit();
	}
}

?>