DROP TABLE accion_grupo;
DROP TABLE usuario;
DROP TABLE accion;
DROP TABLE grupo;
DROP TABLE seccion;


CREATE TABLE  grupo(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    descripcion VARCHAR(500) NOT NULL UNIQUE,
    observaciones TEXT,
    status TINYINT(1) NOT NULL
    )ENGINE=InnoDB;


CREATE TABLE seccion(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    descripcion VARCHAR(500) NOT NULL UNIQUE,
    observaciones TEXT,
    status TINYINT(1) NOT NULL
    )ENGINE=InnoDB;

CREATE TABLE  usuario(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    user VARCHAR(500) NOT NULL UNIQUE,
    password VARCHAR(50) NOT NULL UNIQUE,
    email VARCHAR(500) NOT NULL UNIQUE,
    grupo_id INT(11) NOT NULL,
    FOREIGN KEY (grupo_id) REFERENCES grupo(id)
    ON UPDATE CASCADE ON DELETE RESTRICT
    )ENGINE=InnoDB;

CREATE TABLE  accion(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    descripcion VARCHAR(500) NOT NULL UNIQUE,
    seccion_id INT(11) NOT NULL,
    FOREIGN KEY (seccion_id) REFERENCES seccion(id)
    ON UPDATE CASCADE ON DELETE RESTRICT
    )ENGINE=InnoDB;


CREATE TABLE  accion_grupo(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    accion_id INT(11) NOT NULL,
    grupo_id INT(11) NOT NULL,
    FOREIGN KEY (accion_id) REFERENCES accion(id)
    ON UPDATE CASCADE ON DELETE RESTRICT,
    FOREIGN KEY (grupo_id) REFERENCES grupo(id)
    ON UPDATE CASCADE ON DELETE RESTRICT
    )ENGINE=InnoDB;

ALTER TABLE accion_grupo ADD UNIQUE INDEX accion_grupo (accion_id, grupo_id);

CREATE TABLE  token(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    usuario_id INT(11) NOT NULL,
    grupo_id INT(11) NOT NULL,
    nombre_usuario VARCHAR(500) NOT NULL,
    token TEXT NOT NULL
    )ENGINE=InnoDB;

CREATE TABLE cliente(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(500) NOT NULL,
    apellido_paterno VARCHAR(500) NOT NULL,
    apellido_materno VARCHAR(500),
    telefono VARCHAR(15) NOT NULL,
    email VARCHAR(500) NOT NULL,
    calle VARCHAR(500) NOT NULL,
    numero_ext VARCHAR(10) NOT NULL,
    numero_int VARCHAR(10),
    codigo_postal VARCHAR(50) NOT NULL,
    colonia VARCHAR(50) NOT NULL,
    rfc VARCHAR(50) NOT NULL,
    razon_social VARCHAR(500) NOT NULL,
    nombre_contacto VARCHAR(500) NOT NULL,
    correo_contacto VARCHAR(500) NOT NULL,
    pagina_web VARCHAR(500) NOT NULL,
    banco VARCHAR(50),
    cuenta_bancaria INT(20),
    clabe VARCHAR(500)
)ENGINE=InnoDB;

CREATE TABLE proveedor(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(500) NOT NULL,
    apellido_paterno VARCHAR(500) NOT NULL,
    apellido_materno VARCHAR(500),
    telefono VARCHAR(15) NOT NULL,
    email VARCHAR(500) NOT NULL,
    calle VARCHAR(500) NOT NULL,
    numero_ext VARCHAR(10) NOT NULL,
    numero_int VARCHAR(10),
    codigo_postal VARCHAR(50) NOT NULL,
    colonia VARCHAR(50) NOT NULL,
    rfc VARCHAR(50) NOT NULL,
    razon_social VARCHAR(500) NOT NULL,
    nombre_contacto VARCHAR(500) NOT NULL,
    correo_contacto VARCHAR(500) NOT NULL,
    pagina_web VARCHAR(500) NOT NULL,
    banco VARCHAR(50),
    cuenta_bancaria INT(20),
    clabe VARCHAR(500)
)ENGINE=InnoDB;

CREATE TABLE estado(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    descripcion VARCHAR(500)
)ENGINE=InnoDB;
INSERT INTO estado (descripcion) VALUES ('guia creada');

CREATE TABLE forma_pago(
    id INT(11) NOT NULL PRIMARY KEY,
    descripcion VARCHAR(500) NOT NULL
)ENGINE=InnoDB;


CREATE TABLE guia(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    guia VARCHAR(50) NOT NULL, 
    fecha_entrega DATE NOT NULL,
    calle VARCHAR(500) NOT NULL,
    numero_ext VARCHAR(10) NOT NULL,
    numero_int VARCHAR(10),
    codigo_postal VARCHAR(50) NOT NULL,
    colonia VARCHAR(50) NOT NULL,
    destinatario VARCHAR(50) NOT NULL,
    alto_paquete VARCHAR(500) NOT NULL,
    ancho_paquete VARCHAR(500) NOT NULL,
    largo_paquete VARCHAR(500) NOT NULL,
    peso_paquete VARCHAR(500) NOT NULL,
    descripcion VARCHAR(500) NOT NULL,
    subtotal VARCHAR(50) NOT NULL,
    metodo_pago VARCHAR(50) NOT NULL,
    cliente_id INT(11) NOT NULL,
    estado_id INT(11) NOT NULL ,
    forma_pago_id INT(11) NOT NULL,
    facturado TINYINT(1) NOT NULL,
    timbrado TINYINT(1) NOT NULL,
    FOREIGN KEY (cliente_id) REFERENCES cliente(id)
    ON UPDATE CASCADE ON DELETE RESTRICT,
    FOREIGN KEY (estado_id) REFERENCES estado(id)
    ON UPDATE CASCADE ON DELETE RESTRICT,
    FOREIGN KEY (forma_pago_id) REFERENCES forma_pago(id)
    ON UPDATE CASCADE ON DELETE RESTRICT
)ENGINE=InnoDB;

CREATE TABLE bitacora(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    fecha TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    observaciones VARCHAR(500),
    guia_id INT(11) NOT NULL,
    estado_id INT(11) NOT NULL DEFAULT 1,
    FOREIGN KEY (guia_id) REFERENCES guia(id)
    ON UPDATE CASCADE ON DELETE RESTRICT,
    FOREIGN KEY (estado_id) REFERENCES estado(id)
    ON UPDATE CASCADE ON DELETE RESTRICT
)ENGINE=InnoDB;



---DROP TRIGGER IF EXIST actualiza_guia;
DELIMITER #
CREATE TRIGGER actualiza_guia AFTER INSERT ON bitacora
    FOR EACH ROW
BEGIN
    UPDATE guia SET 
        guia.estado_id=new.estado_id 
    WHERE guia.id = new.guia_id;
END #
DELIMITER ;

INSERT INTO grupo (descripcion, observaciones, status) VALUES ('Admins','Admins',1);
INSERT INTO usuario (user,password,email,grupo_id) VALUES('lily','1234','levangelista@tdesystems.com.mx', 1);

INSERT INTO forma_pago (id,descripcion) VALUES 
(01,'Efectivo'),
(02,'Cheque nominativo'),
(03,'Transferencia electrónica de fondos'),
(04,'Tarjeta de crédito'),  
(05,'Monedero electrónico'),
(06,'Dinero electrónico'),
(08,'Vales de despensa'),
(12,'Dación en pago'),
(13,'Pago por subrogación'),
(14,'Pago por consignación'),
(15,'Condonación'), 
(17,'Compensación'),
(23,'Compensación'),
(24,'Confusión'),
(25,'Remisión de deuda'),
(26,'Prescripción o caducidad'),
(27,'A satisfacción del acreedor'),
(28,'Tarjeta de débito'),
(29,'Tarjeta de servicios'),
(30,'Aplicación de anticipos'),
(99,'Por definir');


