<?php
require_once "config/errores.php";
require_once "config/conexion.php";
class Modelos extends Errores{

	public function activa_db($id, $tabla){
		
		$estructura = $this->genera_estructura();
	    if(!array_key_exists($tabla, $estructura)){ 
	    	$this->error(32,__LINE__,__FILE__);
	    	return false;
		}
		$conexion = new Conexion();
		$conexion->selecciona_base_datos();
		$link = $conexion->link;
		$link->query("UPDATE $tabla SET status = '1' WHERE id = $id");
		if($link->error){
			$this->error(41,__LINE__,__FILE__);
			return false;
		}
		else{

			return True;
		}
	}

	public function alta_db($registro=False, $tabla=False, $nombre_base_datos=False){ //Teminado
		$this->error(-1,__LINE__,__FILE__);
		
		if(is_array($registro)){
			$conexion = new Conexion();
			$conexion->selecciona_base_datos($nombre_base_datos);
			$link = $conexion->link;
		    $estructura = $this->genera_estructura();
		    
		    if(!array_key_exists($tabla, $estructura)){ //errro validado
		    	$this->error(32,__LINE__,__FILE__);
		    }
		    else{
		    	$tabla_base = $estructura[$tabla]['campos'];
		    	if(!$this->valida_campos_nulos($registro, $tabla_base)){
		    		$this->error(37,__LINE__,__FILE__);
		    	}
		    	if(!$this->valida_campos_obligatorios($tabla_base, $registro)){
		    		$this->error(38,__LINE__,__FILE__);
		    	}
		    }
		}
		else{ // error validado
			$this->error(24,__LINE__,__FILE__);
		}
		if($this->numero_error){
			return false;
		}
		else{
			$valores = $this->genera_valores_insercion($tabla_base,$registro);
			$campos = $this->genera_campos_insercion($tabla_base);
			$consulta_insercion = "INSERT INTO ". $tabla." (".$campos.") VALUES (".$valores.")";
			$link->query($consulta_insercion);
			if($link->error){
				$this->error(36,__LINE__,__FILE__);
				return false;
			}
			else{
				$this->error(-1,__LINE__,__FILE__);
				$registro_id = $link->insert_id;
				return $registro_id;
			}
		}
	}

	public function desactiva_db($id, $tabla){
		
		$estructura = $this->genera_estructura();
	    if(!array_key_exists($tabla, $estructura)){ 
	    	$this->error(32,__LINE__,__FILE__);
	    	return false;
		}
		$conexion = new Conexion();
		$conexion->selecciona_base_datos();
		$link = $conexion->link;
		$link->query("UPDATE $tabla SET status = '0' WHERE id = $id");
		if($link->error){
			$this->error(41,__LINE__,__FILE__);
			return false;
		}
		else{

			return True;
		}
	}
		


	public function elimina_db($tabla=False, $id=False, $nombre_base_datos=False){ 
		
		$conexion = new Conexion();
		$conexion->selecciona_base_datos($nombre_base_datos);
		$link = $conexion->link;
		$modelo = new Modelos();

		$database = $modelo->genera_estructura();
		if(!array_key_exists($tabla, $database)){
			$this->error(32,__LINE__,__FILE__);
			return false;
		}
		
		$tabla_base = $database[$tabla]['campos'];
		$consulta = "DELETE FROM ".$tabla. " WHERE id = ".$id;

		$result = $link->query($consulta);

		if($link->error){
			$this->error(40,__LINE__,__FILE__);
			return false;
		}
		return $id;

	}

	public function genera_campo_insercion($campos, $campo, $i, $n_campos){
		$campos = $campos." ".$campo;
		if($i<$n_campos){
			$campos = $campos.",";
		}
		return $campos;
	}

	public function genera_campos_insercion($tabla_base){
		$campos = "";
		$n_campos = count($tabla_base);
		$i = 1;
		foreach ($tabla_base as $campo => $atributos) {
			$campos = $this->genera_campo_insercion($campos, $campo, $i, $n_campos);
			$i++;
		}
		return $campos;		
	}

	public function genera_estructura(){
		$id = array(
			'nombre_campo'=>'id', 'tipo_dato' => 'int(11)', 'nulo' => 'NOT NULL', 'primary' => 'PRIMARY KEY',
			'autoincrement' => 'AUTO_INCREMENT');
		$descripcion = array(
			'nombre_campo'=>'descripcion', 'tipo_dato' => 'varchar(500)', 'nulo' => 'NOT NULL', 
			'primary' => false, 'autoincrement' => false);
		$observaciones = array(
			'nombre_campo'=>'observaciones', 'tipo_dato' => 'text', 'nulo' => false, 'primary' => false, 
			'autoincrement' => false);
		$status = array(
			'nombre_campo'=>'status', 'tipo_dato' => 'tinyint(1)', 'nulo' => false, 'primary' => false, 
			'autoincrement' => false);

		$grupo['nombre_tabla'] = 'grupo';
		$grupo['campos'] = array(
			'id'=>$id, 'descripcion'=>$descripcion, 'observaciones'=>$observaciones, 'status'=>$status);

		$seccion['nombre_tabla'] = 'seccion';
		$seccion['campos'] = array(
			'id'=>$id, 'descripcion'=>$descripcion, 'observaciones'=>$observaciones, 'status'=>$status);

	    $user = array(
	    	'nombre_campo' => 'user','tipo_dato' => 'varchar(500)','nulo' => 'NOT NULL','primary' => false,'autoincrement' => false);
	    $password = array(
	    	'nombre_campo' => 'password','tipo_dato' => 'varchar(50)','nulo' => 'NOT NULL','primary' => false,'autoincrement' => false);
	    $email = array(
	    	'nombre_campo' => 'email','tipo_dato' => 'varchar(500)','nulo' => 'NOT NULL','primary' => false,'autoincrement' => false);
	    $grupo_id = array(
			'nombre_campo'=>'grupo_id', 'tipo_dato' => 'int(11)', 'nulo' => 'NOT NULL', 'primary' => false,
			'autoincrement' => false);

	    $usuario['nombre_tabla'] = 'usuario';
		$usuario['campos'] = array(
			'id'=>$id, 'user'=>$user, 'password'=>$password, 'email'=>$email,'grupo_id'=>$grupo_id);

    	$seccion_id = array(
			'nombre_campo'=>'seccion_id', 'tipo_dato' => 'int(11)', 'nulo' => 'NOT NULL', 'primary' => false,
			'autoincrement' => false);

    	$accion['nombre_tabla'] = 'accion';
		$accion['campos'] = array(
			'id'=>$id, 'descripcion'=>$descripcion, 'seccion_id'=>$seccion_id);

    	$accion_id = array(
			'nombre_campo'=>'accion_id', 'tipo_dato' => 'int(11)', 'nulo' => 'NOT NULL', 'primary' => false,
			'autoincrement' => false);

    	$accion_grupo['nombre_tabla'] = 'accion_grupo';
		$accion_grupo['campos'] = array(
			'id'=>$id, 'accion_id'=>$accion_id, 'grupo_id'=>$grupo_id);

			$token['nombre_tabla'] = 'token';

		$usuario_id = array(
			'nombre_campo'=>'usuario_id', 'tipo_dato' => 'int(11)', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);
		$nombre_usuario = array(
			'nombre_campo'=>'nombre_usuario', 'tipo_dato' => 'varchar(500)', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);
		$token_campo = array(
			'nombre_campo'=>'token', 'tipo_dato' => 'text', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);

		$token['campos'] = array(
			'id' => $id, 'usuario_id' => $usuario_id, 'grupo_id' => $grupo_id, 'nombre_usuario' => $nombre_usuario, 'token' => $token_campo);

		$cliente['nombre_tabla'] = 'cliente';

		$nombre = array(
			'nombre_campo'=>'nombre', 'tipo_dato' => 'varchar(500)', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);
		$apellido_paterno = array(
			'nombre_campo'=>'apellido_paterno', 'tipo_dato' => 'varchar(500)', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);
		$apellido_materno = array(
			'nombre_campo'=>'apellido_materno', 'tipo_dato' => 'varchar(500)', 'nulo' => false, 'primary' => false, 'autoincrement' => false);
		$telefono = array(
			'nombre_campo'=>'telefono', 'tipo_dato' => 'varchar(15)', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);
		$colonia = array(
			'nombre_campo'=>'colonia', 'tipo_dato' => 'varchar(50)', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);
		$codigo_postal = array(
			'nombre_campo'=>'codigo_postal', 'tipo_dato' => 'varchar(50)', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);
		$calle = array(
			'nombre_campo'=>'calle', 'tipo_dato' => 'varchar(500)', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);
		$numero_ext = array(
			'nombre_campo'=>'numero_ext', 'tipo_dato' => 'varchar(10)', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);
		$numero_int = array(
			'nombre_campo'=>'numero_int', 'tipo_dato' => 'varchar(10)', 'nulo' =>false, 'primary' => false, 'autoincrement' => false);
		$rfc = array(
			'nombre_campo'=>'rfc', 'tipo_dato' => 'varchar(50)', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);
		$razon_social = array(
			'nombre_campo'=>'razon_social', 'tipo_dato' => 'varchar(500)', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);
		$nombre_contacto = array(
			'nombre_campo'=>'nombre_contacto', 'tipo_dato' => 'varchar(500)', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);
		$correo_contacto = array(
			'nombre_campo'=>'correo_contacto', 'tipo_dato' => 'varchar(500)', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);
		$pagina_web = array(
			'nombre_campo'=>'pagina_web', 'tipo_dato' => 'varchar(500)', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);
		$banco = array(
			'nombre_campo'=>'banco', 'tipo_dato' => 'varchar(500)', 'nulo' => false, 'primary' => false, 'autoincrement' => false);
		$cuenta_bancaria = array(
			'nombre_campo'=>'cuenta_bancaria', 'tipo_dato' => 'int(20)', 'nulo' => false, 'primary' => false, 'autoincrement' => false);
		$clabe = array(
			'nombre_campo'=>'clabe', 'tipo_dato' => 'varchar(500)', 'nulo' => false, 'primary' => false, 'autoincrement' => false);

		$cliente['campos'] = array(
			'id' => $id, 'nombre' => $nombre, 'apellido_paterno' => $apellido_paterno, 'apellido_materno' => $apellido_materno, 'telefono' => $telefono, 'email' => $email, 'calle' => $calle, 'numero_ext' => $numero_ext, 'numero_int' => $numero_int, 'codigo_postal' => $codigo_postal, 'colonia' => $colonia,'rfc' => $rfc, 'razon_social' => $razon_social, 'nombre_contacto' => $nombre_contacto, 'correo_contacto' => $correo_contacto, 'pagina_web' => $pagina_web, 'banco' => $banco,'cuenta_bancaria' => $cuenta_bancaria, 'clabe' => $clabe); 

		$proveedor['nombre_tabla'] = 'proveedor';

		$proveedor['campos'] = array(
			'id' => $id, 'nombre' => $nombre, 'apellido_paterno' => $apellido_paterno, 'apellido_materno' => $apellido_materno, 'telefono' => $telefono, 'email' => $email, 'calle' => $calle, 'numero_ext' => $numero_ext, 'numero_int' => $numero_int, 'codigo_postal' => $codigo_postal, 'colonia' => $colonia, 'rfc' => $rfc, 'razon_social' => $razon_social, 'nombre_contacto' => $nombre_contacto, 'correo_contacto' => $correo_contacto, 'pagina_web' => $pagina_web, 'banco' => $banco,'cuenta_bancaria' => $cuenta_bancaria, 'clabe' => $clabe); 


		$estado['nombre_tabla'] = 'estado';

		$estado['campos'] = array(
			'id'=>$id, 'descripcion'=>$descripcion);

		$guia['nombre_tabla'] = 'guia';

		$guia = array(
			'nombre_campo'=>'guia', 'tipo_dato' => 'varchar(50)', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);

		$fecha_entrega = array(
			'nombre_campo'=>'fecha_entrega', 'tipo_dato' => 'date', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);
		$destinatario = array(
			'nombre_campo'=>'destinatario', 'tipo_dato' => 'varchar(50)', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);
		$alto_paquete = array(
			'nombre_campo'=>'alto_paquete', 'tipo_dato' => 'varchar(500)', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);
		$ancho_paquete = array(
			'nombre_campo'=>'ancho_paquete', 'tipo_dato' => 'varchar(500)', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);
		$largo_paquete = array(
			'nombre_campo'=>'largo_paquete', 'tipo_dato' => 'varchar(500)', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);
		$peso_paquete = array(
			'nombre_campo'=>'peso_paquete', 'tipo_dato' => 'varchar(500)', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);
		$subtotal = array(
			'nombre_campo'=>'subtotal', 'tipo_dato' => 'varchar(50)', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);

		$cliente_id = array(
			'nombre_campo'=>'cliente_id', 'tipo_dato' => 'int(11)', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);

		$estado_id = array(
			'nombre_campo'=>'estado_id', 'tipo_dato' => 'int(11)', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);
		$forma_pago_id = array(
			'nombre_campo'=>'forma_pago_id', 'tipo_dato' => 'int(11)', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);

		$metodo_pago = array(
			'nombre_campo'=>'metodo_pago', 'tipo_dato' => 'varchar(50)', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);

		$facturado = array(
			'nombre_campo'=>'facturado', 'tipo_dato' => 'tinyint(1)', 'nulo' => false, 'primary' => false, 'autoincrement' => false);

		$timbrado = array(
			'nombre_campo'=>'timbrado', 'tipo_dato' => 'tinyint(1)', 'nulo' =>false , 'primary' => false, 'autoincrement' => false);

		$codigo_barras = array(
			'nombre_campo'=>'codigo_barras', 'tipo_dato' => 'varchar(20)', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);

		$guia['campos'] = array(
			'id' => $id, 'guia'=>$guia, 'fecha_entrega' => $fecha_entrega, 'calle' => $calle, 'numero_ext' => $numero_ext, 'numero_int' => $numero_int, 'codigo_postal' => $codigo_postal, 'colonia' => $colonia, 'destinatario' => $destinatario, 'alto_paquete' => $alto_paquete, 'ancho_paquete' => $ancho_paquete, 'largo_paquete' => $largo_paquete, 'peso_paquete' => $peso_paquete, 'descripcion' => $descripcion, 'subtotal' => $subtotal, 'facturado' => $facturado, 'timbrado' => $timbrado,'cliente_id' => $cliente_id, 'estado_id' => $estado_id, 'forma_pago_id' => $forma_pago_id, 'metodo_pago' => $metodo_pago); 

		$bitacora['nombre_tabla'] = 'bitacora';

		$guia_id = array(
			'nombre_campo'=>'guia_id', 'tipo_dato' => 'int(11)', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);

		$bitacora['campos'] = array(
			'id'=>$id, 'observaciones'=>$observaciones, 'guia_id'=>$guia_id, 'estado_id' => $estado_id);

		$forma_pago['nombre_tabla'] = 'forma_pago';

		$forma_pago['campos'] = array(
			'id'=>$id, 'descripcion'=>$descripcion);



		$database = array('grupo'=>$grupo,'seccion'=>$seccion,'usuario'=>$usuario,'accion'=>$accion,'accion_grupo'=>$accion_grupo, 'token'=>$token, 'cliente'=>$cliente, 'proveedor' => $proveedor, 'estado'=>$estado, 'guia'=>$guia, 'bitacora'=>$bitacora, 'forma_pago'=>$forma_pago);

		return $database;
	}


//---------------------------------------------------------------
	public function genera_lista_accion(){
        $conexion = new Conexion();
        $conexion->selecciona_base_datos();
        $link = $conexion->link;
        $result = $link->query("SELECT a.id,a.descripcion,s.descripcion as descripcion_seccion FROM accion a, seccion s WHERE s.id=a.seccion_id");

        while( $row = mysqli_fetch_assoc( $result)){
		    $new_array[] = $row; // Inside while loop
		}
        if (!empty($new_array)) {
			return $new_array;
		}

    } 

    public function genera_lista_accion_grupo(){
        $conexion = new Conexion();
        $conexion->selecciona_base_datos();
        $link = $conexion->link;
        $result = $link->query("SELECT ag.id,a.descripcion,g.descripcion AS descripcion_grupo FROM accion_grupo ag,accion a,grupo g
        						WHERE (ag.accion_id=a.id) AND (ag.grupo_id=g.id)");

        while( $row = mysqli_fetch_assoc( $result)){
		    $new_array[] = $row; // Inside while loop
		}
		if (!empty($new_array)) {
			return $new_array;
		}
		
    }

    public function genera_lista_bitacora(){
        $conexion = new Conexion();
        $conexion->selecciona_base_datos();
        $link = $conexion->link;
        $result = $link->query("SELECT b.id,g.guia, b.fecha, e.descripcion FROM bitacora b, guia g, estado e
        						WHERE b.guia_id=g.id AND b.estado_id=e.id group by b.id");

        while( $row = mysqli_fetch_assoc( $result)){
		    $new_array[] = $row; // Inside while loop
		}
		if (!empty($new_array)) {
			return $new_array;
		}
		
    }

    	public function genera_lista_cliente(){
        $conexion = new Conexion();
        $conexion->selecciona_base_datos();
        $link = $conexion->link;
        $result = $link->query("SELECT id, CONCAT(nombre,' ',apellido_paterno,' ',apellido_materno) as nombre, telefono, email FROM cliente");

        while( $row = mysqli_fetch_assoc( $result)){
		    $new_array[] = $row; // Inside while loop
		}
		if (!empty($new_array)) {
			return $new_array;
		}
	}

	public function genera_lista_guia(){
        $conexion = new Conexion();
        $conexion->selecciona_base_datos();
        $link = $conexion->link;
        $result = $link->query("SELECT  g.id, g.guia,g.fecha_entrega, g.facturado, CONCAT(nombre,' ',apellido_paterno,' ',apellido_materno) as nombre, e.descripcion FROM guia g, cliente c, estado e WHERE g.cliente_id=c.id AND g.estado_id=e.id order by g.id");

        while( $row = mysqli_fetch_assoc( $result)){
		    $new_array[] = $row;
		}
		if (!empty($new_array)) {
			return $new_array;
		}
	}

	public function genera_lista_grupo(){
        $conexion = new Conexion();
        $conexion->selecciona_base_datos();
        $link = $conexion->link;
        $result = $link->query("SELECT id,descripcion,status FROM grupo");

        while( $row = mysqli_fetch_assoc( $result)){
		    $new_array[] = $row; // Inside while loop
		}
		if (!empty($new_array)) {
			return $new_array;
		}
    }

    public function genera_lista_estado(){
        $conexion = new Conexion();
        $conexion->selecciona_base_datos();
        $link = $conexion->link;
        $result = $link->query("SELECT id,descripcion FROM estado");

        while( $row = mysqli_fetch_assoc( $result)){
		    $new_array[] = $row; // Inside while loop
		}
		if (!empty($new_array)) {
			return $new_array;
		}
    }  

    public function genera_lista_forma_pago(){
        $conexion = new Conexion();
        $conexion->selecciona_base_datos();
        $link = $conexion->link;
        $result = $link->query("SELECT id, descripcion FROM forma_pago");

        while( $row = mysqli_fetch_assoc( $result)){
		    $new_array[] = $row; // Inside while loop
		    
		}
		if (!empty($new_array)) {
			return $new_array;
		}
		
    }

    	public function genera_lista_proveedor(){
        $conexion = new Conexion();
        $conexion->selecciona_base_datos();
        $link = $conexion->link;
        $result = $link->query("SELECT id, CONCAT(nombre,' ',apellido_paterno,' ',apellido_materno) as nombre, telefono, email FROM proveedor");

        while( $row = mysqli_fetch_assoc( $result)){
		    $new_array[] = $row; // Inside while loop
		}
		if (!empty($new_array)) {
			return $new_array;
		}
	}


    public function genera_lista_seccion(){
        $conexion = new Conexion();
        $conexion->selecciona_base_datos();
        $link = $conexion->link;
        $result = $link->query("SELECT id,descripcion,status FROM seccion");

        while( $row = mysqli_fetch_assoc( $result)){
		    $new_array[] = $row; // Inside while loop
		}
		if (!empty($new_array)) {
			return $new_array;
		}

    } 
    public function genera_lista_usuario(){
        $conexion = new Conexion();
        $conexion->selecciona_base_datos();
        $link = $conexion->link;
        $result = $link->query("SELECT u.id,u.user,u.email,g.descripcion FROM usuario u,grupo g WHERE g.id=u.grupo_id");

        while( $row = mysqli_fetch_assoc( $result)){
		    $new_array[] = $row; // Inside while loop
		}
        if (!empty($new_array)) {
			return $new_array;
		}
    } 

    public function genera_lista_session(){
        $conexion = new Conexion();
        $conexion->selecciona_base_datos();
        $link = $conexion->link;
        $result = $link->query("SELECT u.id,u.user,u.password,u.email,g.id AS grupo_id, g.descripcion FROM usuario u,grupo g WHERE g.id=u.grupo_id");

        while( $row = mysqli_fetch_assoc( $result)){
		    $new_array[] = $row; // Inside while loop
		}
        if (!empty($new_array)) {
			return $new_array;
		}
    } 

    	public function genera_lista_token(){
        $conexion = new Conexion();
        $conexion->selecciona_base_datos();
        $link = $conexion->link;
        $result = $link->query("SELECT t.id,t.nombre_usuario,t.token,g.descripcion FROM token t, grupo g WHERE t.grupo_id = g.id");

        while( $row = mysqli_fetch_assoc( $result)){
		    $new_array[] = $row; // Inside while loop
		}
		if (!empty($new_array)) {
			return $new_array;
		}
	}

         
//------------------------------------------------------------------



	public function genera_valor_insercion($campo, $registro, $valores, $i, $n_campos){
		if(!array_key_exists($campo, $registro)){
			if($campo == "id"){
				$valores = $valores."null";	
			}
			else{
				$valores = $valores."''";
			}
		}
		else{
			$valores = $valores."'".$registro[$campo]."'";
		}
		if($i<$n_campos){
			$valores = $valores.",";
		}
		return $valores;
	}

	public function genera_valores_insercion($tabla_base,$registro){
		$valores = "";
		$n_campos = count($tabla_base);
		$i = 1;
		foreach ($tabla_base as $campo => $atributos) {
			$valores = $this->genera_valor_insercion($campo, $registro, $valores, $i, $n_campos);
			$i++;
		}
		return $valores;
	}

	public function modifica_db($registro,$tabla,$nombre_base_datos){
	
		if(is_array($registro)){
			$conexion = new Conexion();
			$conexion->selecciona_base_datos($nombre_base_datos);
			$link = $conexion->link;
		    $estructura = $this->genera_estructura();
		    
		    if(!array_key_exists($tabla, $estructura)){ //errro validado
		    	$this->error(32,__LINE__,__FILE__);
		    }
		    else{
		    	$tabla_base = $estructura[$tabla]['campos'];
		    	if(!$this->valida_campos_nulos($registro, $tabla_base)){
		    		$this->error(37,__LINE__,__FILE__);
		    	}
		    	if(!$this->valida_campos_obligatorios($tabla_base, $registro)){
		    		$this->error(38,__LINE__,__FILE__);
		    	}
		    }
		}
		else{ // error validado
			$this->error(24,__LINE__,__FILE__);
		}
		
		$campos = "";
		$n_campos = count($tabla_base);
		$i = 0;
		foreach ($tabla_base as $campo => $atributos) {
			if($campo !== 'id'){
				if($i<$n_campos){
					$campos.=$campos==""?"$campo='$registro[$campo]'":",$campo='$registro[$campo]'";
				}
			}else{
				$valor_id = $registro[$campo];
				$campo_id = $campo."=".$registro[$campo];
			}

			$i++;
		}
		$consulta_modifica = "UPDATE $tabla SET $campos WHERE $campo_id";
		$link->query($consulta_modifica);
		
		return $valor_id;
	}

	public function obten_por_id($tabla=False, $id=False, $nombre_base_datos=False){ //finalizado
		
		$conexion = new Conexion();
		$conexion->selecciona_base_datos($nombre_base_datos);
		$link = $conexion->link;
		$modelo = new Modelos();
		
		$database = $modelo->genera_estructura();
		if(!array_key_exists($tabla, $database)){
			$this->error(32,__LINE__,__FILE__);
			return false;
		}

		$tabla_base = $database[$tabla]['campos'];
		$consulta = "SELECT *FROM ".$tabla. " WHERE id = ".$id;
		$result = $link->query($consulta);

		if($link->error){
			$this->error(39,__LINE__,__FILE__);
			return false;
		}
		$resultado_envio=array();
		$i=0;
		while($row = $result->fetch_array()){
			foreach ($tabla_base as $campo => $valor) {
				$resultado_envio[$i][$campo] = $row[$campo];
			}
			$i++;
		}

		return $resultado_envio;

	}

	public function valida_campo_nulo($tabla_base, $campo, $valor){
		$no_nulo = $tabla_base[$campo]['nulo'];
    	if($no_nulo){
    		if(!$valor){
				return false;
	    	}
	    	else{
	    		return true;
	    	}
	    }
	    return true;		
	}

	public function valida_campo_obligatorio($valor, $campo, $registro){
		if($valor){
    		if($campo != 'id'){
		    	if(!array_key_exists($campo, $registro)){
		    		return false;
		    	}
		    	else{
		    		return true;
		    	}
			}
		}
		return true;
	}

	public function valida_campos_nulos($registro, $tabla_base){
		foreach ($registro as $campo => $valor) {
    		if(!array_key_exists($campo, $tabla_base)){
		    	$this->error(33,__LINE__,__FILE__);
		    	return false;
		   	}
	 		else{
	    		if(!$this->valida_campo_nulo($tabla_base, $campo, $valor)){
		    		$this->error(34,__LINE__,__FILE__);
		    		return false;	    				
		    	}
		    }
		}
		return true;
	}

	public function valida_campos_obligatorios($tabla_base, $registro){
		foreach ($tabla_base as $campo => $atributos) {	
    		foreach ($atributos as $atributo => $valor) {
		    	if(!$this->valida_campo_obligatorio($valor, $campo, $registro)){
					$this->error(35,__LINE__,__FILE__);
		    		return false;		    				
		    	}
		    }
		}
		return true;		
	}
}
?>