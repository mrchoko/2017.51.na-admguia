
	<form 
		id="form-seccion-alta" name="form-seccion-alta" 
		method="post" 
		action="./index.php?seccion=bitacora&accion=alta_bd">
		<div class="row">&nbsp;</div>
		<div class="form-group row">
			<div class="col-md-6">
				<select name="guia_id" class="selectpicker" data-live-search="true" 
				title="Seleccione Guia" data-width="100%" 
				data-none-results-text="No se encontraron resultados" required>
			        <?php
			        	foreach ($guias as $key => $guia) {
			        		echo '<option value="'.$guia[id].'">'.$guia[guia].'</option>';
			        	}
			        ?>
			    </select>
			    <div class="row">&nbsp;</div>
			</div>
			<div class="col-md-6">
				<select name="estado_id" class="selectpicker" data-live-search="true" 
				title="Seleccione Status Paquete" data-width="100%" 
				data-none-results-text="No se encontraron resultados" required>
			        <?php
			        	foreach ($estados as $key => $estado) {
			        		echo '<option value="'.$estado[id].'">'.$estado[descripcion].'</option>';
			        	}
			        ?>
			    </select>
			</div>
		</div>
		<div class="form-group text-center row">
			<div class="col-md-12">
				<textarea class="form-control noresize" name="observaciones"
				 id="observaciones" rows="3" placeholder="Ingresa Observaciones" 
				 title="Ingrese una Observacion"></textarea>
			</div>
		</div> 
		<div class="form-group text-center row">
			<div class="col-md-12">
				<button type="submit" class="btn btn-secondary" >Enviar</button>
			</div>
		</div>

	</form>
</div>