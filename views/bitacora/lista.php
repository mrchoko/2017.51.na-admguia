
<?php
if (!empty($bitacoras)) {

?>
	<div class="container">
		<div class="row hiden-btn">
			<div class="input-group col-xs-6 col-xs-offset-6">
				<span class="input-group-addon">Busqueda</span>
					<input id="busqueda-1" type="text" class="form-control" placeholder="Ingresa Busqueda">
			</div>
		</div>
		<div class="row hidden">
			<div class="input-group col-xs-12">
				<span class="input-group-addon">Busqueda</span>
					<input id="busqueda-2" type="text" class="form-control" placeholder="Ingresa Busqueda">
			</div>
		</div>
	</div>
	<div class="row">&nbsp;</div>
	<div class="container">
		<div class="row">
			<div class="panel panel-default">
				<div class="table-responsive" id="lista_guia">
				<div class="table-responsive" id="lista_estado">
					<table class="table table-fixed">
    					<thead>
      						<tr>
      							<th class="col-xs-1">Id</th>
        						<th class="col-xs-4">Fecha</th>
        						<th class="col-xs-3 hiden-btn">Numero Guia</th>
        						<th class="col-xs-4 hiden-btn">Status Paquete</th>
        						<th class="col-xs-4 hidden">Guia</th>
        						<th class="col-xs-3 hidden">Status</th>
      						</tr>
    					</thead>
    					<tbody class="registros">
						<?php foreach ($bitacoras as $key => $bitacora) { ?>
							<tr>
								<td class="col-xs-1"><?php echo $bitacora['id']; ?></td>
								<td class="col-xs-4 ocultar-texto"><?php echo $bitacora['fecha']; ?></td>
								<td class="col-xs-3 ocultar-texto"><?php echo $bitacora['guia']; ?></td>
								<td class="col-xs-4 ocultar-texto"><?php echo $bitacora['descripcion']; ?></td>

							</tr>
						<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
}
?>

