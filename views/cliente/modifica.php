	<form 
		id="form-seccion-alta" name="form-seccion-alta" 
		method="post" 
		action="./index.php?seccion=cliente&accion=modifica_bd&cliente_id=<?php echo $cliente_id; ?>">
		<div class="row">&nbsp;</div>
		<div class="form-group row">
			<div class="col-md-4">
				<input 
					type="text" class="form-control" 
					name="nombre" placeholder="Ingresa Nombre" 
					required title="Ingrese un Nombre" 
					value='<?php echo $clientes[0]['nombre']; ?>'>
					<div class="row hidden">&nbsp;</div>
			</div>
			<div class="col-md-4">
				<input 
					type="text" class="form-control" 
					name="apellido_paterno" placeholder="Ingresa Apellido Paterno" 
					required title="Ingrese Apellido Paterno" 
					value='<?php echo $clientes[0]['apellido_paterno']; ?>'>
					<div class="row hidden">&nbsp;</div>
			</div>
			<div class="col-md-4">
				<input 
					type="text" class="form-control" 
					name="apellido_materno" placeholder="Ingresa Apellido Materno" 
					title="Ingrese Apellido Materno" 
					value='<?php echo $clientes[0]['apellido_materno']; ?>'>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-4">
				<input type="text" class="form-control input-medium bfh-phone" 
            	data-format="ddd-ddd-dddd" maxlength="12" placeholder="Ingrese Telefono"
            	name="telefono"
				value='<?php echo $clientes[0]['telefono']; ?>'>
				<div class="row hidden">&nbsp;</div>
			</div>
			<div class="col-md-4">
				<input 
					type="email" class="form-control" 
					name="email" placeholder="Ingresa Correo" 
					required title="Ingrese Correo" 
					value='<?php echo $clientes[0]['email']; ?>'>
					<div class="row hidden">&nbsp;</div>
			</div>
			<div class="col-md-4">
				<input 
					type="text" class="form-control" 
					name="calle" placeholder="Ingresa Calle" 
					required title="Ingrese Calle" 
					value='<?php echo $clientes[0]['calle']; ?>'>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-2">
				<input 
					type="text" class="form-control" 
					name="numero_ext" placeholder="Ingresa Numero Exterior" 
					required title="Ingrese Numero Exterior" 
					value='<?php echo $clientes[0]['numero_ext']; ?>'>
					<div class="row hidden">&nbsp;</div>
			</div>
			<div class="col-md-2">
				<input 
					type="text" class="form-control" 
					name="numero_int" placeholder="Ingresa Numero Interior" 
					title="Ingrese Numero Interior" 
					value='<?php echo $clientes[0]['numero_int']; ?>'>
					<div class="row hidden">&nbsp;</div>
			</div>
			<div class="col-md-4">
				<input 
					type="text" class="form-control input-medium bfh-phone" 
            		data-format="ddddd" maxlength="5" id="codigo_postal" 
					name="codigo_postal" placeholder="Ingresa Codigo Postal" 
					required title="Ingrese Codigo Postal"
					value='<?php echo $clientes[0]['codigo_postal']; ?>'>
					<div class="row hidden">&nbsp;</div>
			</div>
			<div class="col-md-4" id="colonias">
				<input type="hidden" id='valor_colonia' >
					<select id="colonia" name="colonia" style="width:100%;"  required>
						<?php
							for($i=0;$i<sizeof($colonias);$i++){
								echo "<option value='".$colonias[$i]['colonia']."'";
								if($colonias[$i]['colonia'] == $clientes[0]['colonia']){
									echo "selected";
								}
								echo ">".$colonias[$i]['colonia']."</option>";
							}
						?>
			        </select><div class="row hidden">&nbsp;</div>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-4">
					<input 
						type="text" class="form-control" id="municipio" 
						placeholder="Ingresa Municipio" 
						required title="Ingresa Municipio" value="<?php echo $municipios[0]['municipio']?>" disabled>
					<div class="row hidden">&nbsp;</div>
				</div>
			<div class="col-md-4">
				<input 
					type="text" class="form-control" id="estado2" 
					placeholder="Estado" 
					required title="Estado" value="<?php echo $estados[0]['estado']?>" disabled>
					<div class="row hidden">&nbsp;</div>
			</div>
			<div class="col-md-4">
				<input 
					type="text" class="form-control" 
					name="rfc" placeholder="Ingresa RFC" 
					required title="Ingrese RFC" 
					value='<?php echo $clientes[0]['rfc']; ?>'>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-4">
				<input 
					type="text" class="form-control" 
					name="razon_social" placeholder="Ingresa Razon Social"
					required title="Ingrese Razon Social" 
					value='<?php echo $clientes[0]['razon_social']; ?>'>
					<div class="row hidden">&nbsp;</div>
			</div>
			<div class="col-md-4">
				<input 
					type="text" class="form-control" 
					name="nombre_contacto" placeholder="Ingresa Nombre Contacto" 
					required title="Ingrese Nombre Contacto" 
					value='<?php echo $clientes[0]['nombre_contacto']; ?>'>
					<div class="row hidden">&nbsp;</div>
			</div>
			<div class="col-md-4">
				<input 
					type="text" class="form-control" 
					name="correo_contacto" placeholder="Ingresa Correo Contacto" 
					required title="Ingrese Correo Contacto" 
					value='<?php echo $clientes[0]['correo_contacto']; ?>'>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-4">
				<input 
					type="text" class="form-control" 
					name="pagina_web" placeholder="Ingresa Pagina Web" 
					required title="Ingrese Pagina Web" 
					value='<?php echo $clientes[0]['pagina_web']; ?>'>
					<div class="row hidden">&nbsp;</div>
			</div>
			<div class="col-md-4">
				<input 
					type="text" class="form-control" 
					name="banco" placeholder="Ingresa Banco" 
					title="Ingrese Banco" 
					value='<?php echo $clientes[0]['banco']; ?>'>
					<div class="row hidden">&nbsp;</div>
			</div>
			<div class="col-md-4">
				<input 
					type="text" class="form-control" 
					name="cuenta_bancaria" placeholder="Ingresa Cuenta Bancaria" 
					title="Ingrese Cuenta Bncaria" 
					value='<?php echo $clientes[0]['cuenta_bancaria']; ?>'>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-4">
				<input 
					type="text" class="form-control" 
					name="clabe" placeholder="Ingresa Clabe" 
					title="Ingrese Clabe" 
					value='<?php echo $clientes[0]['clabe']; ?>'>
					<div class="row hidden">&nbsp;</div>
			</div>
		
		<div class="form-group text-center row">
			<div class="col-md-12">
				<button type="submit" class="btn btn-secondary" >Enviar</button>
			</div>
		</div>

	</form>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		

		$('#codigo_postal').blur(function(){
			codigo_postal = $("#codigo_postal").val();

			if(codigo_postal != ''){

				$.ajax({
	                url: "./index.php?seccion=cliente&accion=colonia",
	                type: "POST",
	                data: {
	                    codigo_postal: codigo_postal
	                },
	                dataType:'JSON',
	                success: function(response) {

	                	if(response.resultado){
							var colonias='';

							for(var i=0;i<response.colonias.length;i++){
								colonias += "<option value='"+response.colonias[i]['colonia']+"'>"+response.colonias[i]['colonia']+"</option>";
							}

							$('#colonias select').html(colonias).fadeIn();
							$('#municipio').val(response.municipio[0]['municipio']);
							$('#estado2').val(response.estado[0]['estado']);
						}
						else{
							$('#municipio').val('');
							$('#estado2').val('');
						}
	                	
	                },
	                error: function(xhr, status) {
		                alert("Error");
	                }
	            });
			}
			else{
				$('#municipio').val('');
				$('#estado2').val('');
			}

		});

	});
</script>