	<form 
		id="form-seccion-alta" name="form-seccion-alta" 
		method="post" 
		action="./index.php?seccion=guia&accion=alta_bd">
		<div class="row">&nbsp;</div>
		<div class="form-group row">
			<div class="col-md-4">
				<select name="cliente_id" id="cliente_id" class="selectpicker" data-live-search="true" 
				title="Seleccione un Cliente" data-width="100%" 
				data-none-results-text="No se encontraron resultados" required>
			        <?php
			        	foreach ($clientes as $key => $cliente) {
			        		echo '<option value="'.$cliente[id].'">'.$cliente['nombre'].'</option>';
			        	}
			        ?>
			    </select>
				<div class="row hidden">&nbsp;</div>
			</div>
			<div class="col-md-4">
	         	<div class='input-group date' id='divMiCalendario'>
                      <input type='text' id="fecha_entrega" name="fecha_entrega" class="form-control"  readonly/>                 
                      <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                      </span><div class="row hidden">&nbsp;</div>
                </div>
             </div>
			<div class="col-md-4">
				<input 
					type="text" class="form-control" id="calle" 
					name="calle" placeholder="Ingresa calle" 
					required title="Ingrese calle">
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-2">
				<input 
					type="text" class="form-control" id="numero_ext" 
					name="numero_ext" placeholder="Numero Exterior" 
					required title="Numero Exterior">
					<div class="row hidden">&nbsp;</div>
			</div>
			<div class="col-md-2">
				<input 
					type="text" class="form-control" id="numero_int" 
					name="numero_int" placeholder="Numero Interior" 
					title="Numero Interior">
					<div class="row hidden">&nbsp;</div>
			</div>	
			<div class="col-md-4">
				<input 
					type="text" class="form-control input-medium bfh-phone" 
            		data-format="ddddd" maxlength="5" id="codigo_postal" 
					name="codigo_postal" placeholder="Ingresa Codigo Postal" 
					required title="Ingrese Codigo Postal">
					<div class="row hidden">&nbsp;</div>
			</div>
			<div class="col-md-4" id="colonias">
					<select id="colonia" name="colonia" style="width:100%;" required>
			        </select>
			</div>
		</div>
		<div class="form-group row">
				<div class="col-md-4">
					<input 
						type="text" class="form-control" id="municipio" 
						placeholder="Municipio" 
						required title="Municipio" disabled>
					<div class="row hidden">&nbsp;</div>
				</div>
				<div class="col-md-4">
					<input 
						type="text" class="form-control" id="estado2" 
						placeholder="Estado" 
						required title="Estado" disabled>
						<div class="row hidden">&nbsp;</div>
				</div>
				<div class="col-md-4">
					<input 
						type="text" class="form-control" id="destinatario" 
						name="destinatario" placeholder="Nombre Destinatario" 
						required title="Nombre Destinatario">
				</div>
		</div>
		<div class="form-group row">
				<div class="col-md-3">
				<input 
					type="text" class="form-control input-medium bfh-phone" 
            		data-format="dddddddddd" maxlength="10" name="alto_paquete" id="alto_paquete" placeholder="Ingresa Altura" 
					required title="Ingrese Altura">
					<div class="row hidden">&nbsp;</div>
			</div>
			<div class="col-md-3">
				<input 
					type="text" class="form-control input-medium bfh-phone" 
            		data-format="dddddddddd" maxlength="10" name="ancho_paquete" id="ancho_paquete" placeholder="Ingresa Ancho" required title="Ingrese Ancho">
					<div class="row hidden">&nbsp;</div>
			</div>
			<div class="col-md-3">
				<input 
					type="text" class="form-control input-medium bfh-phone" 
            		data-format="dddddddddd" maxlength="10" name="largo_paquete" placeholder="Ingresa Largo" required title="Ingrese Largo  " id="largo_paquete">
					<div class="row hidden">&nbsp;</div>
			</div>
			<div class="col-md-3">
				<input 
					type="text" class="form-control input-medium bfh-phone" 
            		data-format="dddddddddd" maxlength="10" name="peso_paquete" placeholder="Ingresa Peso" required title="Ingrese Peso" id="peso_paquete">
			</div>
		</div>
		<div class="form-group text-center row">
			<div class="col-md-12">
				<textarea class="form-control noresize" name="descripcion"
				 id="descripcion" rows="3" placeholder="Ingresa Descripcion" 
				 required title="Ingrese una Descripcion"></textarea>
			</div>
		</div> 
		<div class="form-group text-center row">
			<div class="col-md-4">
				<input 
					type="text" id="subtotal" name="subtotal" class="form-control"
					placeholder="Subtotal" title="Subtotal" required>
					<div class="row hidden">&nbsp;</div>
			</div>
			<div class="col-md-4">
				<input 
					type="text" class="form-control" id="iva" 
					placeholder="IVA" title="IVA"  disabled> 
					<div class="row hidden">&nbsp;</div>
			</div>
			<div class="col-md-4">
				<input 
					type="text" class="form-control" id="total" 
					placeholder="Total" title="Total" disabled>
			</div>
		</div>
		<div class="form-group text-center row" >
			<div class="col-md-6"  >
                <select name="forma_pago_id" id="forma_pago_id" data-size="5"
                class="selectpicker" data-live-search="true"
                title="Seleccione Forma de Pago" data-width="100%" 
                data-none-results-text="No se encontraron resultados" required>
                <?php
			        	foreach ($formas as $key => $forma_pago) {
			        		echo '<option value="'.$forma_pago[id].'">'.$forma_pago['descripcion'].'</option>';
			        	}
			        ?>
                </select><div class="row hidden">&nbsp;</div>
            </div>	
			<div class="col-md-6">
                <select name="metodo_pago" style="width:100%;" >
                  <option value="">Seleccione Metodo de Pago</option>
                  <option value="PUE">Pago en una sola exhibición</option> 
                  <option value="PPD">Pago en parcialidades o diferido</option>
                </select>
            </div>
		</div>
		<div class="form-group text-center row">
			<div class="col-md-4">
				<input type='hidden' name="guia" />
			</div>
			<div class="col-md-4">
				<input type='hidden' name="estado_id" />
			</div>
			<div class="col-md-4">
				<input type='hidden' name="facturado" />
			</div>
			<div class="col-md-4">
				<input type='hidden' name="timbrado" />
			</div>
			<div class="col-md-12">
				<button type="submit" class="btn btn-secondary" >Enviar</button>
			</div>
		</div>
	</form>
</div> 
<script type="text/javascript">
	$(document).ready(function(){

		$('#codigo_postal').blur(function(){
			codigo_postal = $("#codigo_postal").val();

			if(codigo_postal != ''){
				$.ajax({
	                url: "./index.php?seccion=guia&accion=colonia",
	                type: "POST",
	                data: {
	                    codigo_postal: codigo_postal
	                },
	                dataType:'JSON',
	                success: function(response) {

	                	if(response.resultado){
							var colonias='';

							for(var i=0;i<response.colonias.length;i++){
								colonias += "<option value='"+response.colonias[i]['colonia']+"'>"+response.colonias[i]['colonia']+"</option>";
							}

							$('#colonias select').html(colonias).fadeIn();
							$('#municipio').val(response.municipio[0]['municipio']);
							$('#estado2').val(response.estado[0]['estado']);
						}
						else{
							$('#municipio').val('');
							$('#estado2').val('');
						}
	                },
	                error: function(xhr, status) {
		                alert("Error");
	                }
	            });
			}
			else{
				$('#municipio').val('');
				$('#estado2').val('');
			}

		});
			$('#subtotal').on('keyup', function(){
			var subtotal = eval($('#subtotal').val());
			if(subtotal >=0){
				var iva = subtotal * 0.16;
				var total = subtotal + iva;
				$('#iva').val(iva);
				$('#total').val(total);
			}else{
				$('#iva').val('0');
				$('#total').val('0');
			}
		});
	});
	
</script>