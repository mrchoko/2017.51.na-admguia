<?php
if (!empty($guias)) {

?>
	<div class="container">
		<div class="row hiden-btn">
			<div class="input-group col-xs-6 col-xs-offset-6">
				<span class="input-group-addon">Busqueda</span>
					<input id="busqueda-1" type="text" class="form-control" placeholder="Ingresa Busqueda">
			</div>
		</div>
		<div class="row hidden">
			<div class="input-group col-xs-12">
				<span class="input-group-addon">Busqueda</span>
					<input id="busqueda-2" type="text" class="form-control" placeholder="Ingresa Busqueda">
			</div>
		</div>
	</div>
	<div class="row">&nbsp;</div>
	<div class="container">
		<div class="row">
			<div class="panel panel-default">
				<div class="table-responsive" id="lista_guia">
					<table class="table table-fixed">
    					<thead>
      						<tr>
      							<th class="col-xs-1">Id</th>
        						<th class="col-xs-2">Guia</th>
        						<th class="col-xs-2">Fecha Entrega</th>
        						<th class="col-xs-2">Cliente</th>
        						<th class="col-xs-1">Estado Paquete</th>  	     						
        						<th class="col-xs-4 text-center">Acciones</th>
      						</tr>
    					</thead>
    					<tbody class="registros">
						<?php foreach ($guias as $key => $guia) { ?>
							<tr>
								<td class="col-xs-1"><?php echo $guia['id']; ?></td>
								<td class="col-xs-2 ocultar-texto"><?php echo $guia['guia']; ?></td>
								<td class="col-xs-2 ocultar-texto"><?php echo $guia['fecha_entrega']; ?></td>
								<td class="col-xs-2	 ocultar-texto"><?php echo $guia['nombre']; ?></td>
								<td class="col-xs-1 ocultar-texto"><?php echo $guia['descripcion']; ?></td>

								<td class="col-xs-1">
									<div>
										<?php if($guia['facturado'] == 0){ echo 'Sin Facturar';}
												else{ echo 'Facturado'; } ?>
									</div>
								</td>

								<td class="col-xs-1 hiden-btn">
								<div class="btn-group">
										<a href="index.php?seccion=guia&accion=elimina&guia_id=<?php echo $guia['id']; ?>&codigo_barras=<?php echo $guia['guia'].'.jpg'; ?>">
  											<button type="button" class="btn btn-danger">
  												Elimina
  											</button>
  										</a>
  									</div>
  								</td>
  								<td class="col-xs-1 hiden-btn">
  									<div class="btn-group">
  										<a href="index.php?seccion=guia&accion=genera_factura&guia_id=<?php echo $guia['id']; ?>">
  										<button type="button" class="btn btn-primary">
  											Factura
  										</button>
  										</a>
									</div>
								</td>
								<td class="col-xs-1 hiden-btn">
  									<div class="btn-group">
  										<a href="index.php?seccion=guia&accion=genera_reporte&guia_id=<?php echo $guia['id']; ?>">
  										<button type="button" class="btn btn-primary">
  											Reporte
  										</button>
  										</a>
									</div>
								</td>
								<td class="col-xs-1 hidden"></td>
								<td class="col-xs-2 text-center hidden">

								    <div class="btn-group">
										<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
									    	<span>Acciones</span>
									    	<span></span>
									    	<span class="caret"></span>
										</button>
										<ul class="dropdown-menu dropdown-menu-right" role="menu">
											<li>
												<a href="index.php?seccion=guia&accion=elimina&guia_id=<?php echo $guia['id']; ?>">
		  											<button type="button" class="btn btn-secondary btn-outline">
		  												Elimina
		  											</button>
		  										</a>
											</li>
											<li>
												<a href="index.php?seccion=guia&accion=genera_factura&guia_id=<?php echo $guia['id']; ?>">
		  										<button type="button" class="btn btn-secondary btn-outline">
		  											Genera Factura
		  										</button>
		  										</a>
											</li>
											<li>
												<a href="index.php?seccion=guia&accion=genera_reporte&guia_id=<?php echo $guia['id']; ?>">
		  										<button type="button" class="btn btn-secondary btn-outline">
		  											Genera Reporte
		  										</button>
		  										</a>
											</li>
										</ul>
									</div>
								</td>
							</tr>
						<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
}
?>