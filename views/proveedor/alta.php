	<form 
		id="form-seccion-alta" name="form-seccion-alta" 
		method="post" 
		action="./index.php?seccion=proveedor&accion=alta_bd">
		<div class="row">&nbsp;</div>
		<div class="form-group row">
			<div class="col-md-4">
				<input 
					type="text" class="form-control" 
					name="nombre" placeholder="Ingresa Nombre" 
					required title="Ingrese un Nombre">
					<div class="row hidden">&nbsp;</div>
			</div>
			<div class="col-md-4">
				<input 
					type="text" class="form-control" 
					name="apellido_paterno" placeholder="Ingresa Apellido Paterno" 
					required title="Ingrese Apellido Paterno">
					<div class="row hidden">&nbsp;</div>
			</div>
			<div class="col-md-4">
				<input 
					type="text" class="form-control" 
					name="apellido_materno" placeholder="Ingresa Apellido Materno" 
					title="Ingrese Apellido Materno">
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-4">
            	<input type="text" class="form-control input-medium bfh-phone" 
            	data-format="ddd-ddd-dddd" maxlength="12" placeholder="Ingrese Telefono"
            	name="telefono">
				<div class="row hidden">&nbsp;</div>
			</div>
			<div class="col-md-4">
				<input 
					type="email" class="form-control" 
					name="email" placeholder="Ingresa Correo" 
					required title="Ingrese un correo">
					<div class="row hidden">&nbsp;</div>
			</div>	
			<div class="col-md-4">
				<input 
					type="text" class="form-control" 
					name="calle" placeholder="Ingresa calle" 
					required title="Ingrese calle">
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-2">
				<input 
					type="text" class="form-control" 
					name="numero_ext" placeholder="Numero Exterior" 
					required title="Numero Exterior">
					<div class="row hidden">&nbsp;</div>
			</div>
			<div class="col-md-2">
				<input 
					type="text" class="form-control" 
					name="numero_int" placeholder="Numero Interior" 
					title="Numero Interior">
					<div class="row hidden">&nbsp;</div>
			</div>	
			<div class="col-md-4">
				<input 
					type="text" class="form-control input-medium bfh-phone" 
            		data-format="ddddd" maxlength="5" id="codigo_postal" 
					name="codigo_postal" placeholder="Ingresa Codigo Postal" 
					required title="Ingrese Codigo Postal">
					<div class="row hidden">&nbsp;</div>
			</div>
			<div class="col-md-4" id="colonias">
					<select id="colonia" name="colonia" style="width:100%;"  required>
			        </select>
			</div>
		</div>
		<div class="form-group row">
				<div class="col-md-4">
					<input 
						type="text" class="form-control" id="municipio" 
						placeholder="Ingresa Municipio" 
						required title="Ingresa Municipio" disabled>
					<div class="row hidden">&nbsp;</div>
				</div>
				<div class="col-md-4">
					<input 
						type="text" class="form-control" id="estado2" 
						placeholder="Ingresa Estado" 
						required title="Ingresa Estado" disabled>
					<div class="row hidden">&nbsp;</div>
				</div>
				<div class="col-md-4">
					<input 
						type="text" class="form-control" 
						name="rfc" placeholder="Ingresa RFC" 
						required title="Ingrese RFC">
				</div>
		</div>
		<div class="form-group row">
				<div class="col-md-4">
					<input 
						type="text" class="form-control" 
						name="razon_social" placeholder="Ingresa Razon Social" 
						required title="Ingrese Razon Social">
					<div class="row hidden">&nbsp;</div>
				</div>
				<div class="col-md-4">
					<input 
						type="text" class="form-control" 
						name="nombre_contacto" placeholder="Ingresa Nombre Contacto" 
						required title="Ingrese Nombre Contacto">
					<div class="row hidden">&nbsp;</div>
				</div>
				<div class="col-md-4">
				<input 
					type="email" class="form-control" 
					name="correo_contacto" placeholder="Ingresa Correo Contacto" 
					required title="Ingrese Correo Contacto">
				</div>
		</div>
		<div class="form-group row">
			<div class="col-md-4">
				<input 
					type="text" class="form-control" 
					name="pagina_web" placeholder="Ingresa Pagina Web" 
					required title="Ingrese Pagina Web">
					<div class="row hidden">&nbsp;</div>
			</div>
			<div class="col-md-4">
				<input 
					type="text" class="form-control" 
					name="Banco" placeholder="Ingresa Banco" 
					title="Ingrese Banco">
					<div class="row hidden">&nbsp;</div>
			</div>
			<div class="col-md-4">
				<input 
					type="text" class="form-control" 
					name="cuenta_bancaria" placeholder="Ingresa Cuenta Bancaria" 
					title="Ingrese Cuenta Bancaria">
			</div>
		</div>
		<div class="form-group text-center row">
			<div class="col-md-4">
				<input 
					type="text" class="form-control" 
					name="clabe" placeholder="Ingresa clabe" 
					title="Ingrese Clabe">
			</div>
		</div>
		<div class="form-group text-center row">
			<div class="col-md-12">
				<button type="submit" class="btn btn-secondary" >Enviar</button>
			</div>
		</div>
	</form>
</div>
<script type="text/javascript">
	$(document).ready(function(){

		$('#codigo_postal').blur(function(){
			codigo_postal = $("#codigo_postal").val();

			if(codigo_postal != ''){
				$.ajax({
	                url: "./index.php?seccion=proveedor&accion=colonia",
	                type: "POST",
	                data: {
	                    codigo_postal: codigo_postal
	                },
	                dataType:'JSON',
	                success: function(response) {

	                	if(response.resultado){
							var colonias='';

							for(var i=0;i<response.colonias.length;i++){
								colonias += "<option value='"+response.colonias[i]['colonia']+"'>"+response.colonias[i]['colonia']+"</option>";
							}

							$('#colonias select').html(colonias).fadeIn();
							$('#municipio').val(response.municipio[0]['municipio']);
							$('#estado2').val(response.estado[0]['estado']);
						}
						else{
							$('#municipio').val('');
							$('#estado2').val('');
						}
	                	
	                },
	                error: function(xhr, status) {
		                alert("Error");
	                }
	            });



			}
			else{
				$('#municipio').val('');
				$('#estado2').val('');
			}

		});

	});
</script>