<?php
if (!empty($proveedores)) {

?>
	<div class="container">
		<div class="row hiden-btn">
			<div class="input-group col-xs-6 col-xs-offset-6">
				<span class="input-group-addon">Busqueda</span>
					<input id="busqueda-1" type="text" class="form-control" placeholder="Ingresa Busqueda">
			</div>
		</div>
		<div class="row hidden">
			<div class="input-group col-xs-12">
				<span class="input-group-addon">Busqueda</span>
					<input id="busqueda-2" type="text" class="form-control" placeholder="Ingresa Busqueda">
			</div>
		</div>
	</div>
	<div class="row">&nbsp;</div>
	<div class="container">
		<div class="row">
			<div class="panel panel-default">
				<div class="table-responsive" id="lista_proveedor">
					<table class="table table-fixed">
    					<thead>
      						<tr>
      							<th class="col-xs-1">Id</th>
        						<th class="col-xs-3">Nombre</th>
        						<th class="col-xs-2	">Telefono</th>
        						<th class="col-xs-2">Correo</th>      						
        						<th class="col-xs-3 text-center">Acciones</th>
      						</tr>
    					</thead>
    					<tbody class="registros">
						<?php foreach ($proveedores as $key => $proveedor) { ?>
							<tr>
								<td class="col-xs-1"><?php echo $proveedor['id']; ?></td>
								<td class="col-xs-3 ocultar-texto"><?php echo $proveedor['nombre']; ?></td>
								<td class="col-xs-2 ocultar-texto"><?php echo $proveedor['telefono']; ?></td>
								<td class="col-xs-2 ocultar-texto"><?php echo $proveedor['email']; ?></td>
								<td class="col-xs-2 hiden-btn">
									<div class="btn-group">
										<a href="index.php?seccion=proveedor&accion=elimina&proveedor_id=<?php echo $proveedor['id']; ?>">
  											<button type="button" class="btn btn-danger">
  												Elimina
  											</button>
  										</a>
  									</div>
  								</td>
  								<td class="col-xs-2 hiden-btn">
  									<div class="btn-group">
  										<a href="index.php?seccion=proveedor&accion=modifica&proveedor_id=<?php echo $proveedor['id']; ?>">
  										<button type="button" class="btn btn-primary">
  											Modifica
  										</button>
  										</a>
									</div>
								</td>


								<td class="col-xs-1 hidden"></td>
								<td class="col-xs-3 text-center hidden">

								    <div class="btn-group">
										<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
									    	<span>Acciones</span>
									    	<span></span>
									    	<span class="caret"></span>
										</button>
										<ul class="dropdown-menu dropdown-menu-right" role="menu">
											<li>
												<a href="index.php?seccion=proveedor&accion=elimina&proveedor_id=<?php echo $proveedor['id']; ?>">
		  											<button type="button" class="btn btn-secondary btn-outline">
		  												Elimina
		  											</button>
		  										</a>
											</li>
											<li>
												<a href="index.php?seccion=proveedor&accion=modifica&proveedor_id=<?php echo $proveedor['id']; ?>">
		  										<button type="button" class="btn btn-secondary btn-outline">
		  											Modifica
		  										</button>
		  										</a>
											</li>
										</ul>
									</div>

								</td>


							</tr>
						<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
}
?>